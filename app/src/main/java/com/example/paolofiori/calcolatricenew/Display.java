package com.example.paolofiori.calcolatricenew;

/**
 * Created by paolo.fiori on 17/07/2017.
 */

public class Display {

    public Display(String cifra) {
        this.cifra = cifra;
    }

    private String cifra;

    public String getCifra() {
        return cifra;
    }

    public void setCifra(String cifra) {
        this.cifra = cifra;
    }


}

package com.example.paolofiori.calcolatricenew;

import java.util.ArrayList;
import java.util.EmptyStackException;

/**
 * Created by paolo.fiori on 17/07/2017.
 */

public class Memory extends ArrayList<String> {

    private ArrayList<String> al;
    private StringBuilder allItem;

    public Memory() {
        al = new ArrayList<String>();
    }

    public void push(String item) {
        al.add(item);
    }

    public String pop() {
        if (!isEmpty())
            return al.remove(size()-1);
        else
            throw new EmptyStackException();
    }

    public boolean isEmpty() {
        return (al.size() == 0);
    }

    public String peek() {
        if (!isEmpty())
            return al.get(size()-1);
        else
            throw new EmptyStackException();
    }

    public int size() {
        return al.size();
    }

    @Override
    public String toString() {
        return "MyStack [al=" + al.toString() + "]";

    }

}

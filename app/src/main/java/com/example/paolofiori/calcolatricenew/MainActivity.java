package com.example.paolofiori.calcolatricenew;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView display_line_one;
    TextView display_line_two;

    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button button0;
    Button buttonDot;
    Button buttonCanc;
    Button buttonEqual;
    Button buttonPlus;
    Button buttonMinus;
    Button buttonMult;
    Button buttonDiv;

    Memory memory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display_line_one = (TextView) findViewById(R.id.display_line_one);
        display_line_two = (TextView) findViewById(R.id.display_line_two);
        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        buttonDot = (Button) findViewById(R.id.buttonDot);
        buttonCanc = (Button) findViewById(R.id.buttonCanc);
        buttonPlus = (Button) findViewById(R.id.buttonPlus);
        buttonMinus = (Button) findViewById(R.id.buttonMinus);
        buttonMult = (Button) findViewById(R.id.buttonMult);
        buttonDiv = (Button) findViewById(R.id.buttonDiv);
        buttonEqual = (Button) findViewById(R.id.buttonResult);

        button0.setOnClickListener(this);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        buttonDot.setOnClickListener(this);
        buttonCanc.setOnClickListener(this);
        buttonPlus.setOnClickListener(this);
        buttonMinus.setOnClickListener(this);
        buttonMult.setOnClickListener(this);
        buttonDiv.setOnClickListener(this);
        buttonEqual.setOnClickListener(this);

        memory = new Memory();

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case (R.id.button0):
                display_line_one.append("0");
                memory.push("0");
                break;
            case (R.id.button1):
                display_line_one.append("1");
                memory.push("1");
                break;
            case (R.id.button2):
                display_line_one.append("2");
                memory.push("2");
                break;
            case (R.id.button3):
                display_line_one.append("3");
                memory.push("3");
                break;
            case (R.id.button4):
                display_line_one.append("4");
                memory.push("4");
                break;
            case (R.id.button5):
                display_line_one.append("5");
                memory.push("5");
                break;
            case (R.id.button6):
                display_line_one.append("6");
                memory.push("6");
                break;
            case (R.id.button7):
                display_line_one.append("7");
                memory.push("7");
                break;
            case (R.id.button8):
                display_line_one.append("8");
                memory.push("8");
                break;
            case (R.id.button9):
                display_line_one.append("9");
                memory.push("9");
                break;
            case (R.id.buttonCanc):
                //memory.removeAll();
                break;
            case (R.id.buttonDot):
                display_line_one.append(".");
                memory.push(".");
                break;
            case (R.id.buttonPlus):
                display_line_one.append("+");
                memory.push("+");
                break;
            case (R.id.buttonMinus):
                display_line_one.append("-");
                memory.push("-");
                break;
            case (R.id.buttonMult):
                display_line_one.append("X");
                memory.push("X");
                break;
            case (R.id.buttonDiv):
                display_line_one.append("/");
                memory.push("/");
                break;
            case (R.id.buttonResult):
                    break;

                }
        }
}